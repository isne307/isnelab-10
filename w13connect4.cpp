#include <iostream>
#include <cstdlib>
#include <ctime>
#include <conio.h>
using namespace std;

bool provocation = false;
char input[43];
int PlayOut = 0, EVA = 0;
void board();
int win();
int getValue(int t);
int NegaMax(int depth);
int comp();
void del();
void position(char XO);

int main() {
    srand(time(0));
    del();
    while(true){
        input[comp()]='O';
        system("cls");
        board();
        int win_tmp = win();
        if(win_tmp!=0) {
            if(win_tmp == 1) cout<<endl<<"Player 2 is winner!";
            else if (win_tmp == 2) cout<<endl<<"Player 1 is winner!";
            else if (win_tmp == 3) cout<<"Tie!";
            getch();
            del();
        }else position('X');
    }
}

int NegaMax(int depth) {
    char XO;
    int playNum[8] = {0,0,0,0,0,0,0,0};
    int chance=0;
    if(depth % 2 != 0) XO='X';
    else XO='O';
    for(int col = 1 ; col <= 7 ; col ++) playNum[col]=getValue(col);
    for(int col = 1 ; col <= 7 ; col++) {
        if(playNum[col] != 0) {
            input[playNum[col]]=XO;
            if( win() != 0 ) {
                PlayOut ++;
                if(XO=='O') EVA ++;
                else EVA --;
                input[playNum[col]]=' ';
                return -1;
            }
            input[playNum[col]]=' ';
        }
    }
    if(depth <= 6) {
        for(int col = 1 ; col <= 7 ; col ++) {
            int temp=0;
            if( playNum[col] != 0 ) {
                input[playNum[col]]=XO;
                if( win() != 0 )  {
                    PlayOut++;
                    if(XO=='O') EVA++;
                    else EVA--;
                    input[playNum[col]]=' ';
                    return -1;
                }
                temp = NegaMax(depth+1);
                if(col == 1) chance = temp;
                if(chance < temp) chance = temp;
                input[playNum[col]]=' ';
            }
        }
    }
    return -chance;
}

int comp() {
    float chance[2] = {9999999 , 0 };
    for(int col = 1 ; col <= 7 ; col ++) {
        PlayOut = 0;
        EVA=0;
        int playNum = getValue(col);
        if( playNum != 0 ) {

            input[playNum] = 'O';
            if(win()==2) {
                input[playNum]=' ';
                return playNum ;
            }
            float temp = -(100*NegaMax(1));
            if(PlayOut != 0) temp -= ((100*EVA)/PlayOut);
            if(-temp >= 100) provocation = true;
            if(chance[0] > temp) {
                chance[0] = temp  ;
                chance[1] = playNum;
            }
            input[playNum] = ' ';
        }
    }
    return chance[1];
}

void del(){
    provocation = false;
    for(int i = 0 ; i<= 80 ; i++)
        input[i]=' ';
}

int getValue(int col){
    int n;
    if(col > 7) return 0;
    for(int i = 0 ; i<= 6 ; i++){
        if( input[col+7*i] == ' '  ){
            n = col+7*i;
            break;
        }
    }
    if ( n > 42 ) return 0;
    return n;
}

int win(){
    int temp=0;
    for(int i = 1 ; i<= 42 ; i++){
        if(input[i] != ' '){
            temp++;
            if( i - int((i-1)/7) * 7 <= 4  )
                if( input[i] == input [i+1] && input[i] == input[i+2] && input[i] == input[i+3] )
                    if(input[i] == 'X' ) return 1 ; else return 2;
            if( i <= 21 )
                if ( input[i] == input[i+7] && input[i] == input[i+14] && input[i] == input[i+21]  )
                    if(input[i] == 'X' ) return 1 ; else return 2;
            if( i - int((i-1)/7) * 7 <= 4 && i<=18  )
                if(input[i] == input[i+8] && input[i] == input[i+16] && input[i]==input[i+24])
                    if(input[i] == 'X' ) return 1 ; else return 2;
            if( i - int((i-1)/7) * 7 >= 4 && i <= 21   )
                if(input[i] == input[i+6] && input[i] == input[i+12] && input[i]==input[i+18])
                    if(input[i] == 'X' ) return 1 ; else return 2;
        }
    }
    if (temp == 42) return 3;
    return 0;
}

void board() {
    cout<<endl<<"    1   "<<"    2   "<<"    3   "<<"    4   "<<"    5   "<<"    6   "<<"    7   "<<endl;
    int j = 42;
    for(int i = 0 ; i<= 23 ; i++){
        if(i % 4 == 0) cout<<string(57,'-');
        else{
            if( (i - 2) % 4 == 0){
                j=42-(0.25*i+0.5)*6-((0.25*i+0.5)-1) ;
                for(int i = 0 ; i<=6 ; i++){
                    cout<<"|"<<"   "<<input[j]<<"   ";
                    j++;
                }
                cout<<"|";
            }else{
                for(int i = 0 ; i<=6 ; i++) cout<<"|"<<string(7,' ');
                cout<<"|";
            }
        }
        cout<<endl;
    }
    cout<<string(57,'-');
    if(provocation == true) cout<<endl<<"I'll win"<<endl;
}

void position(char XO){
    int sth;
    cout<<endl<<"where would you like to play"<<endl;
    while(true){
        cin>>sth;
        sth=getValue(sth);
        if( sth != 0 ){
            input[sth] = XO;
            return ;
        }else cout<<"try again"<<endl;
    }
}
